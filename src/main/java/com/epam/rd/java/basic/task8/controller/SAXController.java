package com.epam.rd.java.basic.task8.controller;

import com.epam.rd.java.basic.task8.entity.Flower;
import com.epam.rd.java.basic.task8.entity.flowerTags.GrowingTips;
import com.epam.rd.java.basic.task8.entity.flowerTags.Property;
import com.epam.rd.java.basic.task8.entity.flowerTags.VisualParameters;
import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

import javax.xml.XMLConstants;
import javax.xml.stream.XMLOutputFactory;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.XMLStreamWriter;
import javax.xml.transform.stream.StreamSource;
import javax.xml.validation.Schema;
import javax.xml.validation.SchemaFactory;
import javax.xml.validation.Validator;
import java.io.*;
import java.util.ArrayList;
import java.util.List;

/**
 * Controller for SAX parser.
 */
public class SAXController extends DefaultHandler {

	private String xmlFileName;
	private static final String FLOWERS = "flowers";
	private static final String FLOWER = "flower";
	private static final String NAME = "name";
	private static final String SOIL = "soil";
	private static final String ORIGIN = "origin";
	private static final String VISUAL_PARAMETERS = "visualParameters";
	private static final String STEM_COLOUR = "stemColour";
	private static final String LEAF_COLOUR = "leafColour";
	private static final String AVE_LEN_FLOWER = "aveLenFlower";
	private static final String GROWING_TIPS = "growingTips";
	private static final String TEMPRETURE = "tempreture";
	private static final String LIGHTING = "lighting";
	private static final String WATERING = "watering";
	private static final String MULTIPLYING = "multiplying";
	private List<Flower> flowers;
	private StringBuilder elementValue;
	private VisualParameters visualParameter;
	private GrowingTips growingTips;
	private Property currentProperty;


	public SAXController(String xmlFileName) {
		this.xmlFileName = xmlFileName;
	}

	@Override
	public void startDocument() throws SAXException {

		super.startDocument();
	}

	@Override
	public void startElement(String uri, String localName, String qName, Attributes attributes) throws SAXException {
		switch (qName){
			case FLOWERS:
				flowers = new ArrayList<>();
				break;
			case FLOWER:
				Flower currentFlower = new Flower();
				flowers.add(currentFlower);
				break;
			case NAME:
			case SOIL:
			case ORIGIN:
			case STEM_COLOUR:
			case LEAF_COLOUR:
			case MULTIPLYING:
				elementValue = new StringBuilder();
				break;
			case VISUAL_PARAMETERS:
				visualParameter = new VisualParameters();
				break;
			case AVE_LEN_FLOWER:
			case TEMPRETURE:
			case WATERING:
				currentProperty = new Property();
				currentProperty.setMeasure(attributes.getValue("measure"));
				elementValue = new StringBuilder();
				break;
			case GROWING_TIPS:
				growingTips = new GrowingTips();
				break;
			case LIGHTING:
				elementValue = new StringBuilder(attributes.getValue("lightRequiring"));
				break;


		}
	}

	@Override
	public void endElement(String uri, String localName, String qName) throws SAXException {
		super.endElement(uri, localName, qName);
		switch (qName){
			case NAME:
				latestFlower().setName(elementValue.toString());
				break;
			case SOIL:
				latestFlower().setSoil(elementValue.toString());
				break;
			case ORIGIN:
				latestFlower().setOrigin(elementValue.toString());
				break;
			case VISUAL_PARAMETERS:
				latestFlower().setVisualParameters(visualParameter);
				break;
			case STEM_COLOUR:
				visualParameter.setStemColour(elementValue.toString());
				break;
			case LEAF_COLOUR:
				visualParameter.setLeafColour(elementValue.toString());
				break;
			case AVE_LEN_FLOWER:
				currentProperty.setValue(elementValue.toString());
				visualParameter.setAveLenFlower(currentProperty);
				break;
			case GROWING_TIPS:
				latestFlower().setGrowingTips(growingTips);
				break;
			case TEMPRETURE:
				currentProperty.setValue(elementValue.toString());
				growingTips.setTempreture(currentProperty);
				break;
			case LIGHTING:
				growingTips.setLighting_lightRequiring(elementValue.toString());
				break;
			case WATERING:
				currentProperty.setValue(elementValue.toString());
				growingTips.setWatering(currentProperty);
				break;
			case MULTIPLYING:
				latestFlower().setMultiplying(elementValue.toString());
				break;


		}
	}

	@Override
	public void characters(char[] ch, int start, int length) throws SAXException {
		if (elementValue == null) {
			elementValue = new StringBuilder();
		} else {
			elementValue.append(ch, start, length);
		}
	}
	private Flower latestFlower() {
		List<Flower> flowerList = flowers;
		int latestArticleIndex = flowerList.size() - 1;
		return flowerList.get(latestArticleIndex);
	}

	public List<Flower> getFlowers (){
		return flowers;
	}

	public static void writeXML(String fileName, List<Flower> flowerS){
		OutputStream outputStream = null;
		try {
			outputStream = new FileOutputStream(fileName);
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}

		try {
			XMLStreamWriter out = XMLOutputFactory.newInstance().createXMLStreamWriter(
					new OutputStreamWriter(outputStream, "utf-8"));
			out.writeStartDocument();


			//set NameSpace
			out.writeStartElement("flowers");
			out.setPrefix("xmlns","http://www.nure.ua");
			out.writeNamespace("xmlns", "http://www.nure.ua");
			out.writeNamespace("xsi", "http://www.w3.org/2001/XMLSchema-instance");
			out.writeNamespace("schemaLocation", "http://www.nure.ua input.xsd ");

			for (Flower f: flowerS) {
				//start flower
				out.writeStartElement("flower");

				out.writeStartElement("name");
				out.writeCharacters(f.getName());
				out.writeEndElement();

				out.writeStartElement("soil");
				out.writeCharacters(f.getSoil());
				out.writeEndElement();

				out.writeStartElement("origin");
				out.writeCharacters(f.getOrigin());
				out.writeEndElement();

				//start visualParameters
				out.writeStartElement("visualParameters");
					out.writeStartElement("stemColour");
					out.writeCharacters(f.getVisualParameters().getStemColour());
					out.writeEndElement();

					out.writeStartElement("leafColour");
					out.writeCharacters(f.getVisualParameters().getLeafColour());
					out.writeEndElement();

					out.writeStartElement("aveLenFlower");
					out.writeAttribute("measure",f.getVisualParameters().getAveLenFlower().getMeasure());
					out.writeCharacters(f.getVisualParameters().getAveLenFlower().getValue());
					out.writeEndElement();
				out.writeEndElement();
				//end visualParameters

				//start growingTips
				out.writeStartElement("growingTips");
					out.writeStartElement("tempreture");
					out.writeAttribute("measure",f.getGrowingTips().getTempreture().getMeasure());
					out.writeCharacters(f.getGrowingTips().getTempreture().getValue());
					out.writeEndElement();

					out.writeEmptyElement("lighting");
					out.writeAttribute("lightRequiring",f.getGrowingTips().getLighting_lightRequiring());
					//out.writeEndElement();

					out.writeStartElement("watering");
					out.writeAttribute("measure",f.getGrowingTips().getWatering().getMeasure());
					out.writeCharacters(f.getGrowingTips().getWatering().getValue());
					out.writeEndElement();
				out.writeEndElement();
				//end growingTips
				out.writeStartElement("multiplying");
				out.writeCharacters(f.getMultiplying());
				out.writeEndElement();
				//end flower
				out.writeEndElement();

			}
			//end document
			out.writeEndDocument();

			out.close();



		} catch (XMLStreamException | UnsupportedEncodingException e) {
			e.printStackTrace();
		}
	}

	public static boolean validateXML(String xsdPath, String xmlFile) {
		SchemaFactory factorySchema = SchemaFactory.newInstance(XMLConstants.W3C_XML_SCHEMA_NS_URI);
		Schema schema = null;
		boolean result = false;
		try {
			schema = factorySchema.newSchema(new File(xsdPath));
		} catch (SAXException e) {
			//e.printStackTrace();
			result = false;
		}
		Validator validator;
		if (schema!=null)
			validator = schema.newValidator();
		else return false;
		try {
			validator.validate(new StreamSource(new File(xmlFile)));
			result = true;
		} catch (SAXException e) {
			//e.printStackTrace();
			result = false;
		} catch (IOException e) {
			//e.printStackTrace();
			result = false;
		}
		return result;
	}

}