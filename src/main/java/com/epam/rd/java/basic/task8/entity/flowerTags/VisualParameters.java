package com.epam.rd.java.basic.task8.entity.flowerTags;

public class VisualParameters {
        String stemColour;
        String leafColour;
        Property aveLenFlower;

        public VisualParameters() {
                stemColour = new String();
                leafColour = new String();
                aveLenFlower = new Property();
        }

        public VisualParameters(String stemColour, String leafColour, Property aveLenFlower) {
                this.stemColour = stemColour;
                this.leafColour = leafColour;
                this.aveLenFlower = aveLenFlower;
        }

        public String getStemColour() {
                return stemColour;
        }

        public void setStemColour(String stemColour) {
                this.stemColour = stemColour;
        }

        public String getLeafColour() {
                return leafColour;
        }

        public void setLeafColour(String leafColour) {
                this.leafColour = leafColour;
        }

        public Property getAveLenFlower() {
                return aveLenFlower;
        }

        public void setAveLenFlower(Property aveLenFlower) {
                this.aveLenFlower = aveLenFlower;
        }
}
