package com.epam.rd.java.basic.task8.entity.flowerTags;

public class Property {
    String measure;
    String value;

    public Property() {
    }

    public Property(String measure, String value) {
        this.measure = measure;
        this.value = value;
    }

    public String getMeasure() {
        return measure;
    }

    public void setMeasure(String measure) {
        this.measure = measure;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }
}
