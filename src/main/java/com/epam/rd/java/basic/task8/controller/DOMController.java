package com.epam.rd.java.basic.task8.controller;


import com.epam.rd.java.basic.task8.entity.flowerTags.Property;
import com.epam.rd.java.basic.task8.entity.Flower;
import com.epam.rd.java.basic.task8.entity.flowerTags.GrowingTips;
import com.epam.rd.java.basic.task8.entity.flowerTags.VisualParameters;
import org.w3c.dom.*;
import org.xml.sax.SAXException;

import javax.xml.XMLConstants;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.Source;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import javax.xml.transform.stream.StreamSource;
import javax.xml.validation.Schema;
import javax.xml.validation.SchemaFactory;
import javax.xml.validation.Validator;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

/**
 * Controller for DOM parser.
 */
public class DOMController {

	private final String xmlFileName;

	public DOMController(String xmlFileName) {
		this.xmlFileName = xmlFileName;
	}

	public List<Flower> parseXML () {
		List<Flower> result = new ArrayList<>();
		DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
		DocumentBuilder builder = null;
		try {
			builder = factory.newDocumentBuilder();
		} catch (ParserConfigurationException e) {
			e.printStackTrace();
		}
		try {

			Document document = builder.parse(xmlFileName);
			NodeList flowers = document.getDocumentElement().getElementsByTagName("flower");
			for (int i = 0; i < flowers.getLength(); i++) {
				Node flower = flowers.item(i);
				result.add(getFlower(flower));
			}
		} catch (SAXException | IOException e) {
			e.printStackTrace();
		}
		return result;
	}

	private static Flower getFlower(Node node) {
		Flower flower = new Flower();
		if (node.getNodeType() == Node.ELEMENT_NODE) {
			Element element = (Element) node;
			//name
			flower.setName(getTagValue("name",element));
			//soil
			flower.setSoil(getTagValue("soil",element));
			//origin
			flower.setOrigin(getTagValue("origin",element));
			//visualParameters
			NodeList parameters =  ((Element) node).getElementsByTagName("visualParameters");
			Node visualParameters = parameters.item(0);
			Element vp = (Element) visualParameters;
			getTagProperties("aveLenFlower",vp);
			flower.setVisualParameters(new VisualParameters(getTagValue("stemColour",vp),
															getTagValue("leafColour",vp),
															new Property(getTagProperties("aveLenFlower",vp),getTagValue("aveLenFlower",vp))));
			//growingTips
			NodeList growingTips =  ((Element) node).getElementsByTagName("growingTips");
			Node growingTip = growingTips.item(0);
			Element gt = (Element) growingTip;
			Property temperature = new Property(getTagProperties("tempreture",gt),getTagValue("tempreture",gt));
			Property watering = new Property(getTagProperties("watering",gt),getTagValue("watering",gt));
			String lighting = getTagProperties("lighting",gt);
			flower.setGrowingTips(new GrowingTips(temperature,
												lighting,
												watering));
			//multiplying
			flower.setMultiplying(getTagValue("multiplying",element));

		}
		return flower;
	}

	private static String getTagValue(String tag, Element element) {
		NodeList nodeList = element.getElementsByTagName(tag).item(0).getChildNodes();
		Node node = nodeList.item(0);
		return node.getNodeValue();
	}

	private static String getTagProperties(String tag, Element element) {
		NodeList nodeList = element.getElementsByTagName(tag);
		Node node = nodeList.item(0);
		node.getAttributes().item(0).getNodeValue();
		return node.getAttributes().item(0).getNodeValue();
	}

	public static void sortFlowers (List<Flower> flowers){
		flowers.sort(Comparator.comparing(Flower::getName));
	}

	public static void writeXML(String fileName, List<Flower> flowers){

		DocumentBuilderFactory docFactory = DocumentBuilderFactory.newInstance();
		docFactory.setNamespaceAware(true);
		DocumentBuilder docBuilder = null;
		try {
			docBuilder = docFactory.newDocumentBuilder();
		} catch (ParserConfigurationException e) {
			e.printStackTrace();
		}
		// root elements
		Document doc = docBuilder.newDocument();

			Element root = doc.createElement("flowers");
			root.setAttribute("xmlns", "http://www.nure.ua");
			root.setAttribute("xmlns:xsi", "http://www.w3.org/2001/XMLSchema-instance");
			root.setAttribute("xsi:schemaLocation", "http://www.nure.ua input.xsd ");
		doc.appendChild(root);

		for (Flower f: flowers) {
			Element flower = doc.createElement("flower");
			Element name = doc.createElement("name");
			name.setTextContent(f.getName());
			Element soil = doc.createElement("soil");
			soil.setTextContent(f.getSoil());
			Element origin = doc.createElement("origin");
			origin.setTextContent(f.getOrigin());
			Element visualParameters = doc.createElement("visualParameters");
				Element stemColour = doc.createElement("stemColour");
				stemColour.setTextContent(f.getVisualParameters().getStemColour());
				Element leafColour = doc.createElement("leafColour");
				leafColour.setTextContent(f.getVisualParameters().getStemColour());
				Element aveLenFlower = doc.createElement("aveLenFlower");
				aveLenFlower.setAttribute("measure",f.getVisualParameters().getAveLenFlower().getMeasure());
				aveLenFlower.setTextContent(f.getVisualParameters().getAveLenFlower().getValue());
			Element growingTips = doc.createElement("growingTips");
				Element tempreture = doc.createElement("tempreture");
				tempreture.setAttribute("measure",f.getGrowingTips().getTempreture().getMeasure());
				tempreture.setTextContent(f.getGrowingTips().getTempreture().getValue());
				Element lighting = doc.createElement("lighting");
				lighting.setAttribute("lightRequiring",f.getGrowingTips().getLighting_lightRequiring());
				Element watering = doc.createElement("watering");
				watering.setAttribute("measure",f.getGrowingTips().getWatering().getMeasure());
				watering.setTextContent(f.getGrowingTips().getWatering().getValue());
			Element multiplying = doc.createElement("multiplying");
			multiplying.setTextContent(f.getMultiplying());

			visualParameters.appendChild(stemColour);
			visualParameters.appendChild(leafColour);
			visualParameters.appendChild(aveLenFlower);

			growingTips.appendChild(tempreture);
			growingTips.appendChild(lighting);
			growingTips.appendChild(watering);

			flower.appendChild(name);
			flower.appendChild(soil);
			flower.appendChild(origin);
			flower.appendChild(visualParameters);
			flower.appendChild(growingTips);
			flower.appendChild(multiplying);

			root.appendChild(flower);

		}
		writeDocument(fileName, doc);

	}

	private static void writeDocument(String fileName, Document document) {
		try {
			Transformer tr = TransformerFactory.newInstance().newTransformer();
			DOMSource source = new DOMSource(document);
			FileOutputStream fos = new FileOutputStream(fileName);
			StreamResult result = new StreamResult(fos);
			tr.transform(source, result);
		} catch (TransformerException | IOException e) {
			e.printStackTrace();
		}
	}

	public static boolean validateXML(String xsdPath, String xmlFile) {
		SchemaFactory factorySchema = SchemaFactory.newInstance(XMLConstants.W3C_XML_SCHEMA_NS_URI);
		Schema schema = null;
		boolean result;
		try {
			schema = factorySchema.newSchema(new File(xsdPath));
		} catch (SAXException e) {
			//e.printStackTrace();
		}
		Validator validator;
		if (schema!=null)
			validator = schema.newValidator();
		else return false;
		try {
			validator.validate(new StreamSource(new File(xmlFile)));
			result = true;
		} catch (SAXException e) {
			//e.printStackTrace();
			result = false;
		} catch (IOException e) {
			//e.printStackTrace();
			result = false;
		}
		return result;
	}

}
