package com.epam.rd.java.basic.task8.controller;

import com.epam.rd.java.basic.task8.entity.Flower;
import com.epam.rd.java.basic.task8.entity.flowerTags.GrowingTips;
import com.epam.rd.java.basic.task8.entity.flowerTags.Property;
import com.epam.rd.java.basic.task8.entity.flowerTags.VisualParameters;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

import javax.xml.XMLConstants;
import javax.xml.namespace.QName;
import javax.xml.stream.*;
import javax.xml.stream.events.Attribute;
import javax.xml.stream.events.EndElement;
import javax.xml.stream.events.StartElement;
import javax.xml.stream.events.XMLEvent;
import javax.xml.transform.stream.StreamSource;
import javax.xml.validation.Schema;
import javax.xml.validation.SchemaFactory;
import javax.xml.validation.Validator;
import java.io.*;
import java.util.ArrayList;
import java.util.List;

/**
 * Controller for StAX parser.
 */
public class STAXController extends DefaultHandler {

	private String xmlFileName;


	public STAXController(String xmlFileName) {
		this.xmlFileName = xmlFileName;
	}

	public List<Flower> parseXML(){
		List<Flower> flowers = new ArrayList<>();
		Flower flower = new Flower();
		VisualParameters visualParameters = new VisualParameters();
		GrowingTips growingTips = new GrowingTips();
		XMLInputFactory factory = XMLInputFactory.newInstance();
		try {
			XMLEventReader reader = factory.createXMLEventReader(new FileInputStream(xmlFileName));
			while (reader.hasNext()){
				XMLEvent xmlEvent = reader.nextEvent();
				if (xmlEvent.isStartElement()){
					StartElement startElement = xmlEvent.asStartElement();
					if (startElement.getName().getLocalPart().equals("flower")){
						flower = new Flower();
					} else if (startElement.getName().getLocalPart().equals("name")){
						xmlEvent = reader.nextEvent();
						flower.setName(xmlEvent.asCharacters().getData());
					} else if (startElement.getName().getLocalPart().equals("soil")){
						xmlEvent = reader.nextEvent();
						flower.setSoil(xmlEvent.asCharacters().getData());
					} else if (startElement.getName().getLocalPart().equals("origin")){
						xmlEvent = reader.nextEvent();
						flower.setOrigin(xmlEvent.asCharacters().getData());
					} else if (startElement.getName().getLocalPart().equals("visualParameters")){
						xmlEvent = reader.nextEvent();
						visualParameters = new VisualParameters();
					} else if (startElement.getName().getLocalPart().equals("stemColour")){
						xmlEvent = reader.nextEvent();
						visualParameters.setStemColour(xmlEvent.asCharacters().getData());
					} else if (startElement.getName().getLocalPart().equals("leafColour")){
						xmlEvent = reader.nextEvent();
						visualParameters.setLeafColour(xmlEvent.asCharacters().getData());
					} else if (startElement.getName().getLocalPart().equals("aveLenFlower")){
						xmlEvent = reader.nextEvent();
						Attribute measure = startElement.getAttributeByName(new QName("measure"));
						visualParameters.setAveLenFlower(new Property(measure.getValue(),xmlEvent.asCharacters().getData()));
					} else if (startElement.getName().getLocalPart().equals("growingTips")){
						xmlEvent = reader.nextEvent();
						growingTips = new GrowingTips();
					} else if (startElement.getName().getLocalPart().equals("tempreture")){
						xmlEvent = reader.nextEvent();
						Attribute measure = startElement.getAttributeByName(new QName("measure"));
						growingTips.setTempreture(new Property(measure.getValue(),xmlEvent.asCharacters().getData()));
					} else if (startElement.getName().getLocalPart().equals("lighting")){
						xmlEvent = reader.nextEvent();
						Attribute measure = startElement.getAttributeByName(new QName("lightRequiring"));
						growingTips.setLighting_lightRequiring(measure.getValue());
					} else if (startElement.getName().getLocalPart().equals("watering")){
						xmlEvent = reader.nextEvent();
						Attribute measure = startElement.getAttributeByName(new QName("measure"));
						growingTips.setWatering(new Property(measure.getValue(),xmlEvent.asCharacters().getData()));
					} else if (startElement.getName().getLocalPart().equals("multiplying")){
						xmlEvent = reader.nextEvent();
						flower.setMultiplying(xmlEvent.asCharacters().getData());
					}
				}

				if (xmlEvent.isEndElement()){
					EndElement endElement = xmlEvent.asEndElement();
					if (endElement.getName().getLocalPart().equals("flower")){
						flower.setVisualParameters(visualParameters);
						flower.setGrowingTips(growingTips);
						flowers.add(flower);
					}
				}
			}
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}

		return flowers;
	}

	public static void writeXML(String fileName, List<Flower> flowerS){
		OutputStream outputStream = null;
		try {
			outputStream = new FileOutputStream(fileName);
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}

		try {
			XMLStreamWriter out = XMLOutputFactory.newInstance().createXMLStreamWriter(
					new OutputStreamWriter(outputStream, "utf-8"));
			out.writeStartDocument();


			//set NameSpace
			out.writeStartElement("flowers");
			out.setPrefix("xmlns","http://www.nure.ua");
			out.writeNamespace("xmlns", "http://www.nure.ua");
			out.writeNamespace("xsi", "http://www.w3.org/2001/XMLSchema-instance");
			out.writeNamespace("schemaLocation", "http://www.nure.ua input.xsd ");

			for (Flower f: flowerS) {
				//start flower
				out.writeStartElement("flower");

				out.writeStartElement("name");
				out.writeCharacters(f.getName());
				out.writeEndElement();

				out.writeStartElement("soil");
				out.writeCharacters(f.getSoil());
				out.writeEndElement();

				out.writeStartElement("origin");
				out.writeCharacters(f.getOrigin());
				out.writeEndElement();

				//start visualParameters
				out.writeStartElement("visualParameters");
				out.writeStartElement("stemColour");
				out.writeCharacters(f.getVisualParameters().getStemColour());
				out.writeEndElement();

				out.writeStartElement("leafColour");
				out.writeCharacters(f.getVisualParameters().getLeafColour());
				out.writeEndElement();

				out.writeStartElement("aveLenFlower");
				out.writeAttribute("measure",f.getVisualParameters().getAveLenFlower().getMeasure());
				out.writeCharacters(f.getVisualParameters().getAveLenFlower().getValue());
				out.writeEndElement();
				out.writeEndElement();
				//end visualParameters

				//start growingTips
				out.writeStartElement("growingTips");
				out.writeStartElement("tempreture");
				out.writeAttribute("measure",f.getGrowingTips().getTempreture().getMeasure());
				out.writeCharacters(f.getGrowingTips().getTempreture().getValue());
				out.writeEndElement();

				out.writeEmptyElement("lighting");
				out.writeAttribute("lightRequiring",f.getGrowingTips().getLighting_lightRequiring());
				//out.writeEndElement();

				out.writeStartElement("watering");
				out.writeAttribute("measure",f.getGrowingTips().getWatering().getMeasure());
				out.writeCharacters(f.getGrowingTips().getWatering().getValue());
				out.writeEndElement();
				out.writeEndElement();
				//end growingTips
				out.writeStartElement("multiplying");
				out.writeCharacters(f.getMultiplying());
				out.writeEndElement();
				//end flower
				out.writeEndElement();

			}
			//end document
			out.writeEndDocument();

			out.flush();

			out.close();


		} catch (XMLStreamException | UnsupportedEncodingException e) {
			e.printStackTrace();
		}
	}

	public static boolean validateXML(String xsdPath, String xmlFile) {
		SchemaFactory factorySchema = SchemaFactory.newInstance(XMLConstants.W3C_XML_SCHEMA_NS_URI);
		Schema schema = null;
		boolean result = false;
		try {
			schema = factorySchema.newSchema(new File(xsdPath));
		} catch (SAXException e) {
			//e.printStackTrace();
			result = false;
		}
		Validator validator;
		if (schema!=null)
			validator = schema.newValidator();
		else return false;
		try {
			validator.validate(new StreamSource(new File(xmlFile)));
			result = true;
		} catch (SAXException e) {
			//e.printStackTrace();
			result = false;
		} catch (IOException e) {
			//e.printStackTrace();
			result = false;
		}
		return result;
	}

}