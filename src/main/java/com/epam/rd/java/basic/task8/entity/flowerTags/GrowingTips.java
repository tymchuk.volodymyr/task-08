package com.epam.rd.java.basic.task8.entity.flowerTags;

public class GrowingTips {
        Property tempreture;
        String lighting_lightRequiring;
        Property watering;

        public GrowingTips() {
                tempreture = new Property();
                lighting_lightRequiring = new String();
                watering = new Property();
        }

        public GrowingTips(Property tempreture, String lighting, Property watering) {
                this.tempreture = tempreture;
                this.lighting_lightRequiring = lighting;
                this.watering = watering;
        }

        public Property getTempreture() {
                return tempreture;
        }

        public void setTempreture(Property tempreture) {
                this.tempreture = tempreture;
        }

        public String getLighting_lightRequiring() {
                return lighting_lightRequiring;
        }

        public void setLighting_lightRequiring(String lighting_lightRequiring) {
                this.lighting_lightRequiring = lighting_lightRequiring;
        }

        public Property getWatering() {
                return watering;
        }

        public void setWatering(Property watering) {
                this.watering = watering;
        }
}
