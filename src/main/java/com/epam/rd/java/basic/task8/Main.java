package com.epam.rd.java.basic.task8;

import com.epam.rd.java.basic.task8.controller.*;
import com.epam.rd.java.basic.task8.entity.Flower;

import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

public class Main {
	
	public static void main(String[] args) throws Exception {
		if (args.length != 1) {
			return;
		}
		
		String xmlFileName = args[0];
		System.out.println("Input ==> " + xmlFileName);

		////////////////////////////////////////////////////////
		// DOM
		////////////////////////////////////////////////////////
		
		// get container
		DOMController domController = new DOMController(xmlFileName);
		// PLACE YOUR CODE HERE
		List<Flower> flowersDOM = domController.parseXML();

		// sort (case 1)
		// PLACE YOUR CODE HERE
		DOMController.sortFlowers(flowersDOM);
		// save
		String outputXmlFile = "output.dom.xml";
		String outputInvXmlFile = "invalidXML.xml";
		// PLACE YOUR CODE HERE
		DOMController.writeXML(outputXmlFile,flowersDOM);
		System.out.println(outputXmlFile + " validation with schema input.xsd is " + DOMController.validateXML("input.xsd",outputXmlFile));

		////////////////////////////////////////////////////////
		// SAX
		////////////////////////////////////////////////////////
		
		// get
		SAXController saxController = new SAXController(xmlFileName);
		// PLACE YOUR CODE HERE
		SAXParserFactory factory = SAXParserFactory.newInstance();
		SAXParser saxParser = factory.newSAXParser();

		saxParser.parse(xmlFileName, saxController);
		List<Flower> flowersSAX = saxController.getFlowers();

		// sort  (case 2)
		// PLACE YOUR CODE HERE
		flowersSAX.sort(Comparator.comparing(Flower::getSoil));
		// save
		outputXmlFile = "output.sax.xml";
		// PLACE YOUR CODE HERE

		SAXController.writeXML(outputXmlFile,flowersSAX);
		System.out.println(outputXmlFile + " validation with schema input.xsd is " + SAXController.validateXML("input.xsd",outputXmlFile));
		////////////////////////////////////////////////////////
		// StAX
		////////////////////////////////////////////////////////
		
		// get
		STAXController staxController = new STAXController(xmlFileName);
		// PLACE YOUR CODE HERE
		List<Flower> flowersStAX = staxController.parseXML();
		// sort  (case 3)
		// PLACE YOUR CODE HERE
		flowersStAX.sort(Comparator.comparing(Flower::getOrigin));
		// save
		outputXmlFile = "output.stax.xml";
		STAXController.writeXML(outputXmlFile,flowersStAX);
		System.out.println(outputXmlFile + " validation with schema input.xsd is " + STAXController.validateXML("input.xsd",outputXmlFile));
		// PLACE YOUR CODE HERE
	}

}
